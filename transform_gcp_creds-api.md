## Transforming GCP Credentials service account - TFC API
This repo uses `curl` to set a `GOOGLE_CREDENTIALS` environment variable in the TFC workspace. The Google service account `.json` file must be pre-processed for the curl command to be successful. There are a few ways to perform this transformation:

### Transform using tr and sed (easiest)
```
tr '\n' ' ' < your-gcp-service-account.json | sed -e 's/ //g' -e 's/\"/\\\\"/g' -e 's/\//\\\//g' -e 's/\\n/\\\\\\\\n/g' > your-gcp-service-account.transformed.json
```

### Transform using vi
You can use the following substitution commands in vim:
    1. Hit `Esc` and `:` in vi, then enter `1,$s/\n//`
    1. Enter `:`, then `s/"/\\\\"/g`
    1. Enter `:`, then `s/\//\\\//g`
    1. Enter `:`, then `s/\\n/\\\\\\\\n/g`
    1. Enter `:`, then `wq` to save and quit

Here are the full set of `vi` commands:
```
:1,$s/\n//
:s/"/\\\\"/g
:s/\//\\\//g
:s/\\n/\\\\\\\\n/g
:wq
```

### Transform using your favorite text editor
Alternatively, you can do the following global substitutions in Atom or another text editor:
  - Replace each newline with a blank. (In Atom, click the .* button in the Find control and then replace \n with a blank value. But then deselect the .* button before making the remaining substitutions.)
  - Replace each `"` with `\\"`.
  - Replace each `/` with `\/`.
  - Replace each `\n` with `\\\\n`.

  - Save the path to transformed file in an environment variable:

Now you can use the transformed file with `curl` and TFC API.

Thanks to Roger Berlind for figuring this out in [set-variables-script](https://github.com/hashicorp/terraform-guides/tree/master/operations/variable-scripts#running-the-set-variablessh-script).

